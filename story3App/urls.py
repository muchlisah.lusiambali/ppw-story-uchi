#
from django.urls import path

from .views import (
    index,
    experience,
)

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('',index),
    path("experience/", experience)
]