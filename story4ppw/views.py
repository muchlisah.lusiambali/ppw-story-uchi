from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    return render(request,"index.html")

"""
# Story 1
def story1_index(story1_index):
    return render(story1_index, "story1/index.html")

def description(desc):
    return render(desc, "story1/description.html")

def background(education):
    return render(education, "story1/background.html")

def socialmedia(socmed):
    return render(socmed, "story1/socialmedia.html")

#Story 3
def story3_index(story3_index):
    return render(story3_index,"story3/index.html")

def experience(exp):
    return render(exp, "story3/experience.html")

"""

